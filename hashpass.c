#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h"

int main(int argc, char *argv[] )
{
    
    if (argc < 3)
    {
        printf("please enter 2 files");
        
    }
    FILE *s = fopen(argv[1], "r");
    if (!s)
    {
        perror("Can't open first file\n");
        exit(1);
    }
    
    FILE *d = fopen(argv[2], "w");
    if (!d)
    {
        perror("Can't open second file\n");
        exit(1);
    }
    
    char line[33];
    char* hashes;
    while (fgets(line, 33, s)!=NULL)
    {
    hashes = md5(line, strlen(line)-1);
       fprintf(d, "%s\n", hashes);
       free(hashes);
    }
    
}